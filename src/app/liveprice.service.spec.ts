import { TestBed } from '@angular/core/testing';

import { LivepriceService } from './liveprice.service';

describe('LivepriceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LivepriceService = TestBed.get(LivepriceService);
    expect(service).toBeTruthy();
  });
});
