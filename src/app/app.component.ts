import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Component } from '@angular/core';
import { LivepriceService } from './liveprice.service';




@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  
  stocks:Object[] = [{cat: "Apple"}]
  model
  tradeId

  constructor(private livePrice:LivepriceService){

  }

  invokeService(){
    this.livePrice.getAllTradesData().subscribe( (result) => {
      this.model = result
      console.log(result)
    })
  }

  ngOnInit(){
    this.invokeService()
  }
}
