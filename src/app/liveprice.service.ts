import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class LivepriceService {
  apiURL = "http://localhost:8081/trade/"


  constructor(private http:HttpClient) { }

  private handleError<T> (operation = 'operation', result?: T){
    return (error:any): Observable<T> => {
      console.error(error);
      alert("This item could not be found")
      return throwError(
        "Your request could not be found")
    };
  }

  //getting all trade data
  getAllTradesData(){
    return this.http.get(this.apiURL)
    .pipe(catchError(this.handleError<Object[]>('getData', []))
    );
  }

  // getTradesDataById(tradeId){
  //   return this.http.get(`${this.apiURL}tradeId`).pipe(
  //     catchError(this.handleError<Object[]>('getTradesDataById', []))
  //   );

  //   createTrade(tradeId){
  //     return this.http.post`${this.apiURL}tradeId`).pipe(
  //       catchError(this.handleError<Object[]>('getTradesDataById', []))
  //     );
  }

//}
